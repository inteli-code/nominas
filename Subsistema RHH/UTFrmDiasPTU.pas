unit UTFrmDiasPTU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, UInteliDialog,
  ClientModuleUnit1, DBClient, cxCurrencyEdit, cxCustomPivotGrid, cxDBPivotGrid,
  dxmdaset, cxPivotGridCustomDataSet, cxPivotGridDrillDownDataSet,
  cxGridChartView, cxGridDBChartView, cxPivotGridChartConnection, ExtCtrls,
  cxFilterControl, cxDBFilterControl, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbonCustomizationForm, dxRibbon,
  dxSkinsdxBarPainter, dxBar, dxPSGlbl, dxPSUtl, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSEngn, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPSCore, dxPgsDlg, dxtrprds, dxPSContainerLnk,
  dxPSDBBasedXplorer, dxPSFileBasedXplorer, dxPrnDlg, cxExportPivotGridLink,
  ShellAPI, cxCalendar, cxBarEditItem,DateUtils;

type
  TFrmDiasPtu = class(TForm)
    CdDetalleNOmina: TClientDataSet;
    dsDetalleNomina: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxstyl1: TcxStyle;
    CxGridRepos1: TcxGridViewRepository;
    cxGridChartCxGridRepos1DBChartView1: TcxGridDBChartView;
    CxSerieGridChartCxGridRepos1DBChartView1Series1: TcxGridDBChartSeries;
    CxDataGroupGridChartCxGridRepos1DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    pnl1: TPanel;
    cxstylHeader: TcxStyle;
    cxstylMax: TcxStyle;
    cxstylBackground: TcxStyle;
    DxBManagerPtu: TdxBarManager;
    DxRibbonPtuTab1: TdxRibbonTab;
    DxRibbonPtu: TdxRibbon;
    DxBarOpciones: TdxBar;
    DxBarBtnExportar: TdxBarLargeButton;
    dxComponentPrinter: TdxComponentPrinter;
    dxPSEngineController1: TdxPSEngineController;
    DxPrintDialog2: TdxPrintDialog;
    dxPSDialog1: TdxPageSetupDialog;
    cxDbPGridMain: TcxDBPivotGrid;
    cxPGFieldEmpleado: TcxDBPivotGridField;
    cxPGFieldNominas: TcxDBPivotGridField;
    cxPGFieldDReal: TcxDBPivotGridField;
    cxPGFieldNeto: TcxDBPivotGridField;
    cxPGFieldSueldo: TcxDBPivotGridField;
    cxPGFieldVacaciones: TcxDBPivotGridField;
    dlgSave1: TSaveDialog;
    cxRadioInicio: TcxBarEditItem;
    cxRadioTermino: TcxBarEditItem;
    DxBarBuscar: TdxBar;
    DxBarBtnBuscar: TdxBarLargeButton;
    cxstylTotal: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cxDbPGridMainKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DxBarBtnExportarClick(Sender: TObject);
    procedure DxBarBtnBuscarClick(Sender: TObject);
  private
    //dxCPLink: cxExportPivotGridLink;
    { Private declarations }
  public
    IdPersonal: Integer;
    IdTipoNomina: Integer;
    { Public declarations }
  end;

var
  FrmDiasPtu: TFrmDiasPtu;

implementation

{$R *.dfm}

procedure TFrmDiasPtu.cxDbPGridMainKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  cursor: TCursor;
begin
  try
    Cursor := Screen.Cursor;
    Screen.Cursor := crAppStart;
    if key = 116 then
    begin
      if not cargarDatosFiltrados(cdDetalleNomina, 'ListaPersonal,IdTipoNomina,Anio', [-1,-1,2015]) then
        raise InteligentException.CreateByCode(6, ['IdPersonal', 'Dias PTU', vartostr(IdPersonal)]);

      if CdDetalleNomina.active then
        cdDetalleNomina.Refresh
      else
        cdDetalleNomina.Open;
    end;
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFrmDiasPtu.DxBarBtnBuscarClick(Sender: TObject);
var
  cursor: TCursor;
begin
  try
    Cursor := Screen.Cursor;
    Screen.Cursor := crAppStart;
    if not cargarDatosFiltrados(cdDetalleNomina, 'ListaPersonal,IdTipoNomina,FechaInicio,FechaTermino', [-1,-1,ClientModule1.DatetoStrSql(VarToDateTime(cxRadioInicio.EditValue)), ClientModule1.DatetoStrSql(VarToDateTime(cxRadioTermino.EditValue))]) then
      raise InteligentException.CreateByCode(6, ['IdPersonal', 'Dias PTU', vartostr(IdPersonal)]);

    if CdDetalleNomina.active then
      cdDetalleNomina.Refresh
    else
      cdDetalleNomina.Open;
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFrmDiasPtu.DxBarBtnExportarClick(Sender: TObject);
var
  BasedxReportLink1: TBasedxReportLink;
  rutaArchivo: string;
begin
  dlgSave1.FileName := '';
  if dlgSave1.Execute then
  begin

    rutaArchivo := dlgSave1.FileName + '.xls';
    cxExportPivotGridToExcel(rutaArchivo, cxDbPGridMain);

     if InteliDialog.ShowModal('Aviso', '�Desea abrir el documento exportado?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        ShellExecute(Self.Handle, Nil, PChar(rutaArchivo), '', '', SW_SHOWNORMAL);
  end;
end;

procedure TFrmDiasPtu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFrmDiasPtu.FormCreate(Sender: TObject);
begin
  if not CrearCOnjunto(CdDetalleNomina, 'nom_dias_ptu', ccCatalog) then
    raise InteligentException.CreateByCode(5, ['Dias PTU']);


end;

procedure TFrmDiasPtu.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #116 then
  begin
    if not cargarDatosFiltrados(cdDetalleNomina, 'ListaPersonal,IdTipoNomina', [-1,IdTipoNomina]) then
      raise InteligentException.CreateByCode(6, ['IdPersonal', 'Dias PTU', vartostr(IdPersonal)]);

    if CdDetalleNomina.active then
      cdDetalleNomina.Refresh
    else
      cdDetalleNomina.Open;
  end;

end;

procedure TFrmDiasPtu.FormShow(Sender: TObject);
var
  Cursor: TCursor;
begin
  try
    Cursor := 0;
    Screen.Cursor := CrAppStart;
    try
      if not cargarDatosFiltrados(cdDetalleNomina, 'ListaPersonal,IdTipoNomina', [-1,IdTipoNomina]) then
        raise InteligentException.CreateByCode(6, ['IdPersonal', 'Dias PTU', vartostr(IdPersonal)]);

      if CdDetalleNomina.active then
        cdDetalleNomina.Refresh
      else
        cdDetalleNomina.Open;

      cxRadioInicio.EditValue := DateOf(Now);
      cxRadioTermino.EditValue := DateOf(Now);
    finally
      Screen.Cursor := Cursor;
    end;
  except
    On e: InteligentException do
      InteliDialog.ShowModal(e.title, e.Message, e.msgType, [mbOK], 0);
  end;
end;

end.
